#!/bin/bash
until nc -z -v -w30 db 5432
do
  echo "Waiting for database connection..."
  # wait for 5 seconds before check again
  sleep 5
done

pwd
java -jar /target/order-tracker-0.0.1-SNAPSHOT.jar --spring.profiles.active=dev