FROM maven:3.8.4-jdk-11
WORKDIR /
ADD ./ ./
COPY ./wait_for_mysql.sh /wait_for_mysql.sh
RUN chmod +x /wait_for_mysql.sh
RUN apt-get update && apt-get install -y netcat
RUN mvn clean install -Pdev -DskipTests
ENTRYPOINT ["/wait_for_mysql.sh"]