package com.mthoko.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mthoko.education.entity.*;
import lombok.SneakyThrows;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.mthoko.education.constants.Constants.STANDARD_LOAN_INTEREST_AMOUNT;

public class DummyDataUtil {

   public static CenterLeader getCenterLeader() {
      return new CenterLeader()
              .setName("Mthokozisi")
              .setSurname("Mhlanga");
   }

   public static Center getCenter() {
      List<Member> offeredSubjects = allMembers();
      return new Center()
              .setName("Umlazi/Isipingo")
              .setLevel(Level.Food)
              .setLowestGrade((byte) 8)
              .setHighestGrade((byte) 12)
              .setMembers(offeredSubjects)
              .setDistrict("Umlazi")
              .setAddress("V 1131 Umlazi, KwaZulu-Natal, 4066")
              .setGoogleMapsUrl("https://www.google.com/maps/place/Zwelethu+Secondary+School/@-29.9563436,30.9265755,17z")
              .setTel("0684243087");
   }

   public static SubjectStream getPhysicsStream(List<Member> offeredSubjects) {
      SubjectStream stream = new SubjectStream();
      stream.setName("Food");
      List<String> subjectNames = Arrays.asList("IsiZulu Home Language", "English First Additional Language",
              "Life Orientation", "Mathematics", "Physical Sciences");
      List<Member> subjects = offeredSubjects
              .stream()
              .filter(subject -> subjectNames.contains(subject.getName()))
              .collect(Collectors.toList());
      stream.setMandatorySubjects(subjects);
      return stream;
   }

   @SneakyThrows
   public static List<Member> allMembers() {
      String resourceName = "members.json";
      String data;
      try {
         data = getResourceContents(resourceName);
      } catch (IOException e) {
         e.printStackTrace();
         return new ArrayList<>();
      }
      return List.of(new ObjectMapper().readValue(data, Member[].class)).stream().map(member ->
              member.setLoanInfo(
                      new LoanInfo(member, 0, 0, STANDARD_LOAN_INTEREST_AMOUNT)
              )
      ).collect(Collectors.toList());
   }

   public static String getResourceContents(String s) throws IOException {
      InputStream inputStream = DummyDataUtil.class.getClassLoader()
              .getResourceAsStream(String.format("%s", s));
      return new String(inputStream.readAllBytes());
   }
}
