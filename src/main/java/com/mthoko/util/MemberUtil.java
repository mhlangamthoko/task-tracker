package com.mthoko.util;

import com.mthoko.education.entity.Member;
import com.mthoko.education.repository.MemberRepo;

import static org.apache.logging.log4j.util.Strings.isNotBlank;

public class MemberUtil {

   public static void validateMember(Member member, MemberRepo memberRepo) throws IllegalArgumentException {
      if (!isValid(member)) {
         throw new IllegalArgumentException("Invalid member details");
      }
      if (memberRepo.existsByPhone(member.getPhone())) {
         throw new IllegalArgumentException("Member with phone " + member.getPhone() + " already exists");
      }
   }

   private static boolean isValid(Member member) {
      return member != null &&
              isNotBlank(member.getName()) &&
              isNotBlank(member.getSurname()) &&
              isNotBlank(member.getPhone());
   }
}
