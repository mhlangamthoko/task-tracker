package com.mthoko.education.entity;

import com.mthoko.ordertracker.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@Accessors(chain = true)
public class SubjectStream extends BaseEntity {

   private String name;

   @ManyToMany(fetch = FetchType.EAGER)
   private List<Member> mandatorySubjects = new ArrayList<>();

}