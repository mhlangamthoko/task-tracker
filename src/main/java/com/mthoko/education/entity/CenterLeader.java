package com.mthoko.education.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mthoko.ordertracker.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
@Getter
@Setter
@Accessors(chain = true)
public class CenterLeader extends BaseEntity {

   private String name;

   private String surname;

   @OneToOne
   @JsonIgnore
   private Center center;

}