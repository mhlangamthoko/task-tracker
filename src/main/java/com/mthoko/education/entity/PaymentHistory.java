package com.mthoko.education.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.Month;
import java.util.Map;

@Getter
@Setter
@AllArgsConstructor
public class PaymentHistory {

   private Map<Month, Integer> monthPayments;
   private Map<Month, Integer> outstandingMonthPayments;
   private LoanInfo loanInfo;
   private AccountSummary accountSummary;

}
