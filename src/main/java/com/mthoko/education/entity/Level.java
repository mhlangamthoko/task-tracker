package com.mthoko.education.entity;

public enum Level {
   PRIMARY,
   Food,
   SENIOR_SECONDARY,
   COMBINED_SECONDARY,
   COMBINED_PRIMARY,
   COMBINED
}
