package com.mthoko.education.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mthoko.ordertracker.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
@Getter
@Setter
@Accessors(chain = true)
public class ExclusiveSubject extends BaseEntity {

   @ManyToOne
   @JoinColumn(name = "subject_stream_id")
   @JsonIgnore
   private SubjectStream subjectStream;

   @OneToMany
   @LazyCollection(LazyCollectionOption.FALSE)
   private List<Member> availableSubjects;

}