package com.mthoko.education.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AccountSummary {
   private int totalMonthlyRepayments;
   private int totalMonthlyRepaymentsOwing;
   private int totalLoanBorrowed;
   private int totalLoanRepayments;
   private int totalLoanOwing;
   private int totalAmountOwing;
   private int totalAmountPayed;
}
