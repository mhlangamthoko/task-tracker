package com.mthoko.education.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mthoko.ordertracker.entity.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LoanInfo extends BaseEntity {

   @JsonIgnore
   @OneToOne
   private Member member;

   private Integer totalBorrowedAmount;

   private Integer totalPaidAmount;

   private Integer outstandingAmount;

}
