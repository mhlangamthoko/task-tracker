package com.mthoko.education.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mthoko.ordertracker.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@Accessors(chain = true)
public class Payment extends BaseEntity {

   @JsonIgnore
   @ManyToOne
   @JoinColumn(name = "member_id", nullable = false, updatable = false)
   private Member member;

   private Integer amount;

   @ManyToOne
   @JoinColumn(name = "target_id", nullable = false)
   private PaymentTarget target;

   @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
   private LocalDateTime dateCaptured;

}