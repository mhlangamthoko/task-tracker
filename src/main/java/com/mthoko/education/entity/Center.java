package com.mthoko.education.entity;

import com.mthoko.ordertracker.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.CascadeType.ALL;

@Entity
@Getter
@Setter
@Accessors(chain = true)
public class Center extends BaseEntity {

   private String name;

   private String address;

   private String tel;

   private String district;

   private String googleMapsUrl;

   private Level level;

   private byte lowestGrade;

   private byte highestGrade;

   @OneToOne(optional = false, orphanRemoval = true, cascade = ALL)
   private CenterLeader leader;

   @OneToMany(mappedBy = "center", cascade = ALL)
   @LazyCollection(LazyCollectionOption.FALSE)
   private List<Member> members = new ArrayList<>();

}