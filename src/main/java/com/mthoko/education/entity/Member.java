package com.mthoko.education.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mthoko.ordertracker.entity.BaseEntity;
import lombok.*;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static javax.persistence.CascadeType.ALL;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class Member extends BaseEntity {

   @Column(nullable = false)
   private String name;

   @Column(nullable = false)
   private String surname;

   @Column(unique = true, nullable = false)
   private String phone;

   @JsonIgnore
   @ManyToOne
   @JoinColumn(name = "center_id", updatable = false)
   private Center center;

   @OneToMany(mappedBy = "member", cascade = ALL)
   private List<Payment> payments = new ArrayList<>();

   @OneToOne(cascade = ALL)
   private LoanInfo loanInfo;

}