package com.mthoko.education.service;

import com.mthoko.education.entity.CenterLeader;

import java.util.List;

public interface LeaderService {

   List<CenterLeader> findAll();

   void deleteById(Long id);
}
