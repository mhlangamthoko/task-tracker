package com.mthoko.education.service;

import com.mthoko.education.entity.Center;
import com.mthoko.education.entity.Member;

import java.util.List;
import java.util.Optional;

public interface CenterService {

   List<Center> findAll();

   Center save(Center center);

   Optional<Center> findById(Long id);

   void deleteAll();

   Center update(Center center);

   void deleteById(Long id);

   Member addMember(Member member, Long centerId);
}
