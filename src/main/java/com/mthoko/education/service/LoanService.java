package com.mthoko.education.service;

import com.mthoko.education.entity.Borrowal;

import java.util.List;

public interface LoanService {
   List<Borrowal> findByMemberId(Long memberId);

   void deleteById(Long id);

   Borrowal captureBorrowal(Long memberId, Borrowal borrowal);
}
