package com.mthoko.education.service.impl;

import com.mthoko.education.entity.Borrowal;
import com.mthoko.education.entity.LoanInfo;
import com.mthoko.education.entity.Member;
import com.mthoko.education.repository.BorrowalRepo;
import com.mthoko.education.repository.LoanInfoRepo;
import com.mthoko.education.repository.MemberRepo;
import com.mthoko.education.service.LoanService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

import static com.mthoko.education.constants.Constants.MAX_LOAN_AMOUNT;

@Service
@RequiredArgsConstructor
public class LoanServiceImpl implements LoanService {
   private final BorrowalRepo borrowalRepo;
   private final MemberRepo memberRepo;
   private final LoanInfoRepo loanInfoRepo;

   @Override
   public List<Borrowal> findByMemberId(Long memberId) {
      return borrowalRepo.findByMember_Id(memberId);
   }

   @Override
   public void deleteById(Long id) {
      borrowalRepo.deleteById(id);
   }

   @Override
   public Borrowal captureBorrowal(Long memberId, Borrowal borrowal) {
      if (!memberRepo.existsById(memberId)) {
         throw new IllegalArgumentException(String.format("Member with id %s not found", memberId));
      }
      if (borrowal.getDateCaptured() == null) {
         borrowal.setDateCaptured(LocalDateTime.now());
      }
      Member member = memberRepo.findById(memberId).orElseThrow();
      LoanInfo loanInfo = member.getLoanInfo();
      int maxLegibleAmount = MAX_LOAN_AMOUNT - loanInfo.getTotalBorrowedAmount();
      if (borrowal.getAmount() > maxLegibleAmount) {
         throw new IllegalArgumentException(
                 String.format("Request could not be completed. Max possible borrowal amount is %s",
                         maxLegibleAmount)
         );
      }
      loanInfo.setTotalBorrowedAmount(loanInfo.getTotalBorrowedAmount() + borrowal.getAmount());
      loanInfoRepo.save(loanInfo);
      return borrowalRepo.save(borrowal.setMember(member));
   }
}
