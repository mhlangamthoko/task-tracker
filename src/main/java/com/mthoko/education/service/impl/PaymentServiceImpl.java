package com.mthoko.education.service.impl;

import com.mthoko.education.entity.*;
import com.mthoko.education.repository.LoanInfoRepo;
import com.mthoko.education.repository.MemberRepo;
import com.mthoko.education.repository.PaymentRepo;
import com.mthoko.education.repository.PaymentTargetRepo;
import com.mthoko.education.service.PaymentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.mthoko.education.constants.Constants.*;

@Service
@RequiredArgsConstructor
public class PaymentServiceImpl implements PaymentService {

   private final PaymentRepo paymentRepo;
   private final MemberRepo memberRepo;
   private final PaymentTargetRepo paymentTargetRepo;
   private final LoanInfoRepo loanInfoRepo;

   @Override
   public List<Payment> findByMemberId(Long memberId) {
      return paymentRepo.findByMember_Id(memberId);
   }

   @Override
   public List<Payment> findByMemberIdAndTargetType(Long memberId, String targetType) {
      return paymentRepo.findByMember_IdAndTarget_Type(memberId, targetType);
   }

   @Override
   public PaymentHistory findPaymentHistoryByMemberId(Long memberId) {
      List<Payment> payments = findByMemberId(memberId);
      int totalAmount = payments.stream().filter(payment -> MONTH_TYPE.equals(payment.getTarget().getType())).mapToInt(payment -> payment.getAmount()).sum();
      int monthsSettled = totalAmount / MONTHLY_PAYMENT;
      int lastMonthPartialAmount = totalAmount % MONTHLY_PAYMENT;
      Map<Month, Integer> monthlyPayments = getMonthlyPaymentsMade(monthsSettled);
      Map<Month, Integer> outstandingPayments = getOutstandingMonthlyPayments(monthsSettled);
      if (lastMonthPartialAmount > 0 && monthsSettled < Month.DECEMBER.getValue()) {
         monthlyPayments.put(Month.of(++monthsSettled), lastMonthPartialAmount);
         outstandingPayments.put(Month.of(monthsSettled), lastMonthPartialAmount - MONTHLY_PAYMENT);
      }
      LoanInfo loanInfo = loanInfoRepo.findByMember_Id(memberId);
      if (loanInfo != null) {
         int loanPayment = payments.stream()
                 .filter(payment -> LOAN_TYPE.equals(payment.getTarget().getType()))
                 .mapToInt(payment -> payment.getAmount())
                 .sum();
         loanInfo.setTotalPaidAmount(loanPayment);
         loanInfo.setOutstandingAmount(loanInfo.getTotalBorrowedAmount() + STANDARD_LOAN_INTEREST_AMOUNT - loanPayment);
      }
      return new PaymentHistory(monthlyPayments, outstandingPayments, loanInfo, mapSummary(monthlyPayments, outstandingPayments, loanInfo));
   }

   private AccountSummary mapSummary(Map<Month, Integer> monthlyPayments, Map<Month, Integer> outstandingPayments, LoanInfo loanInfo) {
      int totalMonthlyRepayments = monthlyPayments.values().stream().mapToInt(value -> value).sum();
      int totalMonthlyRepaymentsOwing = outstandingPayments.values().stream().mapToInt(value -> -value).sum();
      return AccountSummary.builder()
              .totalMonthlyRepayments(totalMonthlyRepayments)
              .totalMonthlyRepaymentsOwing(totalMonthlyRepaymentsOwing)
              .totalLoanBorrowed(loanInfo.getTotalBorrowedAmount())
              .totalLoanRepayments(loanInfo.getTotalPaidAmount())
              .totalLoanOwing(loanInfo.getOutstandingAmount())
              .totalAmountOwing(loanInfo.getOutstandingAmount() + totalMonthlyRepaymentsOwing)
              .totalAmountPayed(loanInfo.getTotalPaidAmount() + totalMonthlyRepayments)
              .build();
   }

   private Map<Month, Integer> getOutstandingMonthlyPayments(int monthsSettled) {
      return sortByMonth(
              IntStream
                      .rangeClosed(monthsSettled + 1, 12)
                      .mapToObj(monthIntValue -> Month.of(monthIntValue))
                      .collect(Collectors.toMap(month -> month, month -> -MONTHLY_PAYMENT))
      );
   }

   private Map<Month, Integer> getMonthlyPaymentsMade(int monthsSettled) {
      return sortByMonth(
              IntStream
                      .rangeClosed(1, Integer.min(monthsSettled, 12))
                      .mapToObj(monthIntValue -> Month.of(monthIntValue))
                      .collect(Collectors.toMap(month -> month, month -> MONTHLY_PAYMENT))
      );
   }

   private Map<Month, Integer> sortByMonth(Map<Month, Integer> map) {
      TreeMap<Month, Integer> sorted = new TreeMap<>(Comparator.comparing(Month::getValue));
      sorted.putAll(map);
      return sorted;
   }

   @Override
   @Transactional
   public Payment save(Payment payment) {
      return paymentRepo.save(payment);
   }

   @Override
   public Optional<Payment> findById(Long id) {
      return paymentRepo.findById(id);
   }

   @Override
   @Transactional
   public void deleteAll() {
      paymentRepo.deleteAll();
   }

   @Override
   @Transactional
   public Payment capturePayment(Long memberId, Payment payment) {
      if (!memberRepo.existsById(memberId)) {
         throw new IllegalArgumentException(String.format("Member with id %s not found", memberId));
      }
      if (payment.getDateCaptured() == null) {
         payment.setDateCaptured(LocalDateTime.now());
      }
      payment
              .setTarget(getPaymentTarget(payment))
              .setMember(memberRepo.findById(memberId).get());
      Payment saved = save(payment);
      if (LOAN_TYPE.equals(payment.getTarget().getType())) {
         LoanInfo loanInfo = loanInfoRepo.findByMember_Id(memberId);
         if (loanInfo == null) {
            loanInfo = new LoanInfo();
         }
      }
      return saved;
   }

   private PaymentTarget getPaymentTarget(Payment payment) {
      return paymentTargetRepo
              .findByType(payment.getTarget().getType())
              .orElseThrow(() ->
                      new IllegalArgumentException(String.format("type %s not found", payment.getTarget().getType()))
              );
   }

   @Override
   @Transactional
   public void deleteById(Long id) {
      if (paymentRepo.existsById(id)) {
         paymentRepo.deleteById(id);
      }
   }

}
