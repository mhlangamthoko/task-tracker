package com.mthoko.education.service.impl;

import com.mthoko.education.entity.*;
import com.mthoko.education.repository.CenterRepo;
import com.mthoko.education.repository.MemberRepo;
import com.mthoko.education.repository.PaymentTargetRepo;
import com.mthoko.education.service.CenterService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Optional;

import static com.mthoko.education.constants.Constants.*;
import static com.mthoko.util.DummyDataUtil.*;
import static com.mthoko.util.MemberUtil.validateMember;

@Service
@RequiredArgsConstructor
public class CenterServiceImpl implements CenterService {

   private final CenterRepo centerRepo;
   private final PaymentTargetRepo paymentTargetRepo;
   private final MemberRepo memberRepo;

   @PostConstruct
   private void postConstruct() {
      if (centerRepo.count() == 0) {
         saveInitialStaticData();
      }
   }

   private void saveInitialStaticData() {
      paymentTargetRepo.saveAll(List.of(new PaymentTarget(LOAN_TYPE), new PaymentTarget(MONTH_TYPE)));
      List<Member> members = allMembers();
      CenterLeader centerLeader = getCenterLeader();
      Center center = getCenter()
              .setLeader(centerLeader)
              .setMembers(members);
      centerLeader.setCenter(center);
      members.forEach(member -> member.setCenter(center));
      save(center);
   }

   @Override
   public List<Center> findAll() {
      return centerRepo.findAll();
   }

   @Override
   @Transactional
   @javax.transaction.Transactional
   public Center save(Center center) {
      if (center.getLeader() == null) {
         throw new IllegalArgumentException("Leader must be allocated");
      }
      center.getLeader().setCenter(center);
      center.getMembers().forEach(
              member -> member
                      .setCenter(center)
                      .getLoanInfo()
                      .setMember(member)
      );
      return centerRepo.save(center);
   }

   @Override
   public Optional<Center> findById(Long id) {
      return centerRepo.findById(id);
   }

   @Override
   @Transactional
   public void deleteAll() {
      centerRepo.deleteAll();
   }

   @Override
   @Transactional
   public Center update(Center center) {
      if (centerRepo.existsById(center.getId())) {
         return save(center);
      }
      return center;
   }

   @Override
   @Transactional
   public void deleteById(Long id) {
      if (centerRepo.existsById(id)) {
         centerRepo.deleteById(id);
      }
   }

   @Override
   public Member addMember(Member member, Long centerId) {
      Optional<Center> optionalCenter = centerRepo.findById(centerId);
      if (optionalCenter.isPresent()) {
         validateMember(member, memberRepo);
         Center center = optionalCenter.get();
         member.setCenter(center);
         center.getMembers().add(member);
         member.setLoanInfo(new LoanInfo(member, 0, 0, STANDARD_LOAN_INTEREST_AMOUNT));
         centerRepo.save(center);
      }
      return member;
   }

}
