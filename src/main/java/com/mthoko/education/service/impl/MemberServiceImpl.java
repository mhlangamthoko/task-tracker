package com.mthoko.education.service.impl;

import com.mthoko.education.entity.Center;
import com.mthoko.education.entity.LoanInfo;
import com.mthoko.education.entity.Member;
import com.mthoko.education.entity.SubjectStream;
import com.mthoko.education.repository.CenterRepo;
import com.mthoko.education.repository.ExclusiveSubjectRepo;
import com.mthoko.education.repository.MemberRepo;
import com.mthoko.education.repository.StreamRepo;
import com.mthoko.education.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.mthoko.education.constants.Constants.STANDARD_LOAN_INTEREST_AMOUNT;

@Service
@RequiredArgsConstructor
public class MemberServiceImpl implements MemberService {

   private final MemberRepo memberRepo;

   private final ExclusiveSubjectRepo exclusiveSubjectRepo;

   private final StreamRepo streamRepo;

   private final CenterRepo centerRepo;

   @Override
   public List<Member> findAll() {
      return memberRepo.findAll();
   }

   @Override
   public Member save(Member member) {
      member.setLoanInfo(new LoanInfo(member, 0, 0, STANDARD_LOAN_INTEREST_AMOUNT));
      return memberRepo.save(member);
   }

   @Override
   public Optional<Member> findById(Long id) {
      return memberRepo.findById(id);
   }

   @Override
   @Transactional
   public SubjectStream createStream(SubjectStream subjectStream) {
      final SubjectStream stream = streamRepo.save(subjectStream);
      memberRepo.saveAll(stream.getMandatorySubjects());
      return stream;
   }

   @Override
   public Optional<SubjectStream> findStreamById(Long id) {
      return streamRepo.findById(id);
   }

   @Override
   @Transactional
   public void deleteAll() {
      List<Center> all = centerRepo.findAll();
      all.forEach(center -> center.setMembers(new ArrayList<>()));
      centerRepo.saveAll(all);
      exclusiveSubjectRepo.deleteAll();
      streamRepo.deleteAll();
      memberRepo.deleteAll();
   }

   @Override
   public void deleteById(Long id) {
      memberRepo.deleteById(id);
   }

   @Override
   public Optional<Member> findByPhone(String phone) {
      return memberRepo.findByPhone(phone);
   }

   @Override
   public List<Member> findByCenterId(Long centerId) {
      return memberRepo.findByCenter_Id(centerId);
   }

}
