package com.mthoko.education.service.impl;

import com.mthoko.education.entity.CenterLeader;
import com.mthoko.education.repository.CenterLeaderRepo;
import com.mthoko.education.service.LeaderService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class LeaderServiceImpl implements LeaderService {

   private final CenterLeaderRepo centerLeaderRepo;

   @Override
   public List<CenterLeader> findAll() {
      return centerLeaderRepo.findAll();
   }

   @Override
   public void deleteById(Long id) {
      centerLeaderRepo.deleteById(id);
   }
}
