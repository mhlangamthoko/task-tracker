package com.mthoko.education.service;

import com.mthoko.education.entity.Member;
import com.mthoko.education.entity.SubjectStream;

import java.util.List;
import java.util.Optional;

public interface MemberService {

   List<Member> findAll();

   Member save(Member member);

   Optional<Member> findById(Long id);

   SubjectStream createStream(SubjectStream stream);

   Optional<SubjectStream> findStreamById(Long id);

   void deleteAll();

   void deleteById(Long id);

   Optional<Member> findByPhone(String phone);

   List<Member> findByCenterId(Long centerId);
}
