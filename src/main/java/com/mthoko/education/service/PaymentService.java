package com.mthoko.education.service;

import com.mthoko.education.entity.Payment;
import com.mthoko.education.entity.PaymentHistory;

import java.util.List;
import java.util.Optional;

public interface PaymentService {

   Payment save(Payment payment);

   Optional<Payment> findById(Long id);

   void deleteAll();

   Payment capturePayment(Long memberId, Payment payment);

   void deleteById(Long id);

   List<Payment> findByMemberId(Long memberId);

   List<Payment> findByMemberIdAndTargetType(Long memberId, String targetType);

   PaymentHistory findPaymentHistoryByMemberId(Long memberId);
}
