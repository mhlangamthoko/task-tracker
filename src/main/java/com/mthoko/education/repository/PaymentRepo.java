package com.mthoko.education.repository;

import com.mthoko.education.entity.Payment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PaymentRepo extends JpaRepository<Payment, Long> {

   List<Payment> findByMember_Id(Long memberId);

   List<Payment> findByMember_IdAndTarget_Type(Long memberId, String targetType);
}
