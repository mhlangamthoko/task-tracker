package com.mthoko.education.repository;

import com.mthoko.education.entity.SubjectStream;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StreamRepo extends JpaRepository<SubjectStream, Long> {
}
