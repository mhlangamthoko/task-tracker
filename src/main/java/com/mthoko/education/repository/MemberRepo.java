package com.mthoko.education.repository;

import com.mthoko.education.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MemberRepo extends JpaRepository<Member, Long> {
   Optional<Member> findByPhone(String phone);

   List<Member> findByCenter_Id(Long centerId);

   boolean existsByPhone(String phone);
}
