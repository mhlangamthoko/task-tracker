package com.mthoko.education.repository;

import com.mthoko.education.entity.LoanInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LoanInfoRepo extends JpaRepository<LoanInfo, Long> {
   LoanInfo findByMember_Id(Long memberId);
}
