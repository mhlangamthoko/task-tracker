package com.mthoko.education.repository;

import com.mthoko.education.entity.PaymentTarget;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PaymentTargetRepo extends JpaRepository<PaymentTarget, Long> {
   Optional<PaymentTarget> findByType(String type);
}
