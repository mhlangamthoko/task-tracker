package com.mthoko.education.repository;

import com.mthoko.education.entity.CenterLeader;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CenterLeaderRepo extends JpaRepository<CenterLeader, Long> {
   void deleteByCenterLeader_id(Long id);
}
