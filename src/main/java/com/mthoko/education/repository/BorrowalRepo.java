package com.mthoko.education.repository;

import com.mthoko.education.entity.Borrowal;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BorrowalRepo extends JpaRepository<Borrowal, Long> {
   List<Borrowal> findByMember_Id(Long memberId);
}
