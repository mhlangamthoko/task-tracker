package com.mthoko.education.repository;

import com.mthoko.education.entity.ExclusiveSubject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExclusiveSubjectRepo extends JpaRepository<ExclusiveSubject, Long> {
}
