package com.mthoko.education.controller;

import com.mthoko.education.entity.Member;
import com.mthoko.education.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("member")
@RequiredArgsConstructor
public class MemberController {

   private final MemberService memberService;

   @GetMapping
   public List<Member> findAll() {
      return memberService.findAll();
   }

   @GetMapping("{id}")
   public Optional<Member> findById(@PathVariable Long id) {
      return memberService.findById(id);
   }

   @GetMapping("phone/{phone}")
   public Optional<Member> findByPhone(@PathVariable String phone) {
      return memberService.findByPhone(phone);
   }

   @DeleteMapping("{id}")
   public void deleteById(@PathVariable("id") Long id) {
      memberService.deleteById(id);
   }

   @PostMapping("add")
   public Member addMember(@RequestBody Member member) {
      return memberService.save(member);
   }

   @GetMapping("centerId/{centerId}")
   public List<Member> findByCenterId(@PathVariable Long centerId) {
      return memberService.findByCenterId(centerId);
   }

}
