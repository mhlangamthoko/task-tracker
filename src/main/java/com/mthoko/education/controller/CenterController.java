package com.mthoko.education.controller;

import com.mthoko.education.entity.Center;
import com.mthoko.education.entity.Level;
import com.mthoko.education.entity.Member;
import com.mthoko.education.service.CenterService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("center")
@RequiredArgsConstructor
public class CenterController {

   private final CenterService centerService;

   @GetMapping
   public List<Center> findAll() {
      return centerService.findAll();
   }

   @PostMapping("create")
   public Center save(@RequestBody Center center) {
      return centerService.save(center);
   }

   @GetMapping("levels")
   public List<Level> getAllLevels() {
      return Stream.of(Level.values())
              .collect(Collectors.toList());
   }

   @PutMapping
   public Center update(@RequestBody Center center) {
      return centerService.update(center);
   }

   @DeleteMapping("{id}")
   @ResponseStatus(HttpStatus.OK)
   public void deleteById(@PathVariable("id") Long id) {
      centerService.deleteById(id);
   }

   @GetMapping("{id}")
   public Optional<Center> findById(@PathVariable("id") Long id) {
      return centerService.findById(id);
   }

   @PostMapping("{centerId}/addMember")
   public Member addMember(@RequestBody Member member, @PathVariable Long centerId) {
      return centerService.addMember(member, centerId);
   }

}
