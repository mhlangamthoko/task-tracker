package com.mthoko.education.controller;

import com.mthoko.education.entity.Payment;
import com.mthoko.education.entity.PaymentHistory;
import com.mthoko.education.service.PaymentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("payment")
@RequiredArgsConstructor
public class PaymentController {

   private final PaymentService paymentService;

   @GetMapping("memberId/{memberId}")
   public List<Payment> findByMemberId(@PathVariable Long memberId) {
      return paymentService.findByMemberId(memberId);
   }

   @DeleteMapping("{id}")
   public void deleteById(@PathVariable("id") Long id) {
      paymentService.deleteById(id);
   }

   @PostMapping("capture/{memberId}")
   public Payment capturePayment(@PathVariable Long memberId, @RequestBody Payment payment) {
      return paymentService.capturePayment(memberId, payment);
   }

   @GetMapping(value = "history/{memberId}", produces = MediaType.APPLICATION_JSON_VALUE)
   public PaymentHistory paymentHistory(@PathVariable Long memberId) {
      return paymentService.findPaymentHistoryByMemberId(memberId);
   }

}
