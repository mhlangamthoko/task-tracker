package com.mthoko.education.controller;

import com.mthoko.education.entity.CenterLeader;
import com.mthoko.education.service.LeaderService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("member/leader")
@RequiredArgsConstructor
public class CenterLeaderController {

   private final LeaderService leaderService;

   @GetMapping
   public List<CenterLeader> findAll() {
      return leaderService.findAll();
   }

   @DeleteMapping("{id}")
   public void deleteById(@PathVariable("id") Long id) {
      leaderService.deleteById(id);
   }
}
