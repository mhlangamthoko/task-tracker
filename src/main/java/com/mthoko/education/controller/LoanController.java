package com.mthoko.education.controller;

import com.mthoko.education.entity.Borrowal;
import com.mthoko.education.service.LoanService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("loan")
@RequiredArgsConstructor
public class LoanController {

   private final LoanService loanService;

   @GetMapping("memberId/{memberId}")
   public List<Borrowal> findByMemberId(@PathVariable Long memberId) {
      return loanService.findByMemberId(memberId);
   }

   @DeleteMapping("{id}")
   public void deleteById(@PathVariable("id") Long id) {
      loanService.deleteById(id);
   }

   @PostMapping("capture/{memberId}")
   public Borrowal captureBorrowal(@PathVariable Long memberId, @RequestBody Borrowal borrowal) {
      return loanService.captureBorrowal(memberId, borrowal);
   }

}
