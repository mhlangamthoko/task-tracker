package com.mthoko.education.controller.advice;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.time.LocalDateTime;

@ControllerAdvice
public class CustomControllerAdvice {


   @Getter
   @Setter
   private static class InternalErrorDto {
      private String errorMessage;
      @JsonFormat(pattern = "YYYY-MM-dd HH:mm:ss")
      private final LocalDateTime timeStamp = LocalDateTime.now();

      public InternalErrorDto(String errorMessage) {
         setErrorMessage(errorMessage);
      }
   }

   @ExceptionHandler(
           RuntimeException.class
   )
   public ResponseEntity<Object> handleRuntimeException(
           Exception ex, WebRequest request) {
      return new ResponseEntity<>(
              new InternalErrorDto(ex.getMessage()),
              new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR
      );
   }

}
