package com.mthoko.education.constants;

public interface Constants {

   String LOAN_TYPE = "loan";
   String MONTH_TYPE = "month";
   int MONTHLY_PAYMENT = 300;
   Integer STANDARD_LOAN_INTEREST_AMOUNT = 1400;
   Integer MAX_LOAN_AMOUNT = 1000;
}
