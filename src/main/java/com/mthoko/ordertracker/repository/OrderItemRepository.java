package com.mthoko.ordertracker.repository;

import com.mthoko.ordertracker.entity.OrderItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderItemRepository extends JpaRepository<OrderItem, Long> {
    void deleteByOrder_Id(long id);
}
