package com.mthoko.ordertracker.controller;

import com.mthoko.ordertracker.entity.Product;
import com.mthoko.ordertracker.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("products")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;

    @GetMapping
    public List<Product> findAll() {
        return productService.findAll();
    }

    @PostMapping
    public List<Product> saveAll(@RequestBody List<Product> orders) {
        return productService.saveAll(orders);
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteById(@PathVariable("id") long id) {
        productService.deleteById(id);
    }

    @PostMapping("create")
    public Product createProduct(@RequestBody Product product) {
        return productService.createProduct(product);
    }
}
