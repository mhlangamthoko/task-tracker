package com.mthoko.ordertracker.controller;

import com.mthoko.ordertracker.entity.Order;
import com.mthoko.ordertracker.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("orders")
@RequiredArgsConstructor
public class OrderController {

    private final OrderService orderService;

    @GetMapping
    public List<Order> findAll() {
        return orderService.findAll();
    }

    @PostMapping
    public List<Order> saveAll(@RequestBody List<Order> orders) {
        return orderService.saveAll(orders);
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteById(@PathVariable("id") long id) {
        orderService.deleteById(id);
    }

    @DeleteMapping("all")
    @ResponseStatus(HttpStatus.OK)
    public void deleteAll() {
        orderService.deleteAll();
    }

    @PostMapping("create")
    public Order createOrder(@RequestBody Order order) {
        return orderService.createOrder(order);
    }

    @PutMapping("toggleReminder/{id}")
    public Optional<Order> toggleReminder(@PathVariable("id") long id) {
        return orderService.toggleViaPhone(id);
    }

}
