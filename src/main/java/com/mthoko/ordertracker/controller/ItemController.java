package com.mthoko.ordertracker.controller;

import com.mthoko.ordertracker.entity.OrderItem;
import com.mthoko.ordertracker.service.OrderItemService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("items")
@RequiredArgsConstructor
public class ItemController {

    private final OrderItemService orderItemService;

    @GetMapping
    public List<OrderItem> findAll() {
        return orderItemService.findAll();
    }
}
