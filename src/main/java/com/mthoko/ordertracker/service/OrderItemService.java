package com.mthoko.ordertracker.service;

import com.mthoko.ordertracker.entity.OrderItem;

import java.util.List;

public interface OrderItemService {
    List<OrderItem> findAll();
}
