package com.mthoko.ordertracker.service.impl;

import com.mthoko.ordertracker.entity.Order;
import com.mthoko.ordertracker.entity.OrderItem;
import com.mthoko.ordertracker.repository.OrderItemRepository;
import com.mthoko.ordertracker.repository.OrderRepository;
import com.mthoko.ordertracker.service.OrderService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

    private final OrderRepository orderRepository;
    private final OrderItemRepository orderItemRepository;

    @Override
    public List<Order> findAll() {
        return orderRepository.findAll();
    }

    @Override
    @Transactional
    public List<Order> saveAll(List<Order> orders) {
        return orderRepository.saveAll(orders);
    }

    @Override
    @Transactional
    public void deleteById(long id) {
        if (orderRepository.existsById(id)) {
            orderItemRepository.deleteByOrder_Id(id);
            orderRepository.deleteById(id);
        }
    }

    @Override
    @Transactional
    public void deleteAll() {
        orderItemRepository.deleteAll();
        orderRepository.deleteAll();
    }

    @Override
    @Transactional
    public void deleteAll(List<Order> orders) {
        List<OrderItem> items = orders.stream()
                .map(order -> order.getItems())
                .reduce(new ArrayList<>(), (orderItems, orderItems2) -> {
                    orderItems.addAll(orderItems2);
                    return orderItems;
                });
        orderItemRepository.deleteAll(items);
        orderRepository.deleteAll(orders);
    }

    @Override
    public boolean existsById(Long id) {
        return orderRepository.existsById(id);
    }

    @Override
    public Optional<Order> findById(Long id) {
        return orderRepository.findById(id);
    }

    @Override
    @Transactional
    public Optional<Order> toggleViaPhone(long id) {
        Optional<Order> optionalOrder = orderRepository.findById(id);
        if (optionalOrder.isPresent()) {
            Order order = optionalOrder.get();
            order.setViaPhone(!order.isViaPhone());
            orderRepository.save(order);
        }
        return optionalOrder;
    }

    @Override
    @Transactional
    public Order createOrder(Order order) {
        Order saved = orderRepository.save(order);
        List<OrderItem> orderItems = order.getItems()
                .stream()
                .map(orderItem -> orderItem.setOrder(saved))
                .collect(Collectors.toList());
        orderItemRepository.saveAll(orderItems);
        return saved;
    }
}
