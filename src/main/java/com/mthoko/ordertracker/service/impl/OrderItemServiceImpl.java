package com.mthoko.ordertracker.service.impl;

import com.mthoko.ordertracker.entity.OrderItem;
import com.mthoko.ordertracker.repository.OrderItemRepository;
import com.mthoko.ordertracker.service.OrderItemService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class OrderItemServiceImpl implements OrderItemService {

    private final OrderItemRepository orderItemRepo;

    @Override
    public List<OrderItem> findAll() {
        return orderItemRepo.findAll();
    }
}
