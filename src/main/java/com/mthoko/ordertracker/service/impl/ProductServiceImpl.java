package com.mthoko.ordertracker.service.impl;

import com.mthoko.ordertracker.entity.Product;
import com.mthoko.ordertracker.repository.ProductRepository;
import com.mthoko.ordertracker.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepo;

    @Override
    public List<Product> findAll() {
        return productRepo.findAll();
    }

    @Override
    @Transactional
    public List<Product> saveAll(List<Product> products) {
        return productRepo.saveAll(products);
    }

    @Override
    @Transactional
    public void deleteById(long id) {
        if (productRepo.existsById(id)) {
            productRepo.deleteById(id);
        }
    }

    @Override
    public boolean existsById(Long id) {
        return productRepo.existsById(id);
    }

    @Override
    public Optional<Product> findById(Long id) {
        return productRepo.findById(id);
    }

    @Override
    @Transactional
    public Product createProduct(Product product) {
        if (product.getId() != null && productRepo.existsById(product.getId())) {
            return productRepo.findById(product.getId()).get();
        }
        return productRepo.save(product);
    }
}
