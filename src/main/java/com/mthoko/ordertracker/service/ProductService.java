package com.mthoko.ordertracker.service;

import com.mthoko.ordertracker.entity.Product;

import java.util.List;
import java.util.Optional;

public interface ProductService {
    List<Product> findAll();

    List<Product> saveAll(List<Product> products);

    void deleteById(long id);

    boolean existsById(Long id);

    Optional<Product> findById(Long id);

    Product createProduct(Product product);
}
