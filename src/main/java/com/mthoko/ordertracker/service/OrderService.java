package com.mthoko.ordertracker.service;

import com.mthoko.ordertracker.entity.Order;

import java.util.List;
import java.util.Optional;

public interface OrderService {

    List<Order> findAll();

    List<Order> saveAll(List<Order> orders);

    void deleteById(long id);

    void deleteAll();

    void deleteAll(List<Order> orders);

    boolean existsById(Long id);

    Optional<Order> findById(Long id);

    Optional<Order> toggleViaPhone(long id);

    Order createOrder(Order order);
}
