package com.mthoko.ordertracker.entity;

import lombok.Data;

import javax.persistence.Entity;

@Entity
@Data
public class Product extends BaseEntity {
    private String description;
    private int price;
}