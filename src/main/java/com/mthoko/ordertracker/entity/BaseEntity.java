package com.mthoko.ordertracker.entity;

import lombok.Data;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
@Data
public class BaseEntity {

   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private Long id;

   @Override
   public boolean equals(Object that) {
      if (that == null || !that.getClass().equals(this.getClass())) {
         return false;
      }
      return id == ((BaseEntity) that).id;
   }
}