package com.mthoko.ordertracker.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "order_table")
@Data
public class Order extends BaseEntity {
    private String customerName;
    private String customerNumber;
    private boolean viaPhone;
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "order")
    private List<OrderItem> items = new java.util.ArrayList<>();
}
