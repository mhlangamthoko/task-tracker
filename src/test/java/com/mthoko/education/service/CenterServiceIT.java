package com.mthoko.education.service;

import com.mthoko.education.entity.Center;
import com.mthoko.education.entity.CenterLeader;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static com.mthoko.util.DummyDataUtil.getCenter;
import static com.mthoko.util.DummyDataUtil.getCenterLeader;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class CenterServiceIT {

   @Autowired
   private CenterService centerService;

   @BeforeEach
   void flushData() {
      centerService.deleteAll();
   }

   @Test
   void testSaveCenter() {
      Center center = getCenter();
      center.setLeader(getCenterLeader());
      centerService.save(center);
      assertEquals(1, centerService.findAll().size());
      assertNotNull(center.getId());
      Optional<Center> saved = centerService.findById(center.getId());
      assertTrue(saved.isPresent());
      Center retrievedCenter = saved.get();
      CenterLeader savedCenterLeader = retrievedCenter.getLeader();
      assertNotNull(savedCenterLeader);
      assertEquals(center, retrievedCenter);
      assertEquals(center.getLeader(), savedCenterLeader);
   }

   @Test
   void testSaveCenterWithoutLeader() {
      Center center = getCenter();
      assertThrows(IllegalArgumentException.class, () -> centerService.save(center));
      assertNull(center.getId());
      assertEquals(0, centerService.findAll().size());
   }

}
