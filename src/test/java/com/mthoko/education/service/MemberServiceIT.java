package com.mthoko.education.service;

import com.mthoko.education.entity.ExclusiveSubject;
import com.mthoko.education.entity.Member;
import com.mthoko.education.entity.SubjectStream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@RunWith(SpringRunner.class)
@SpringBootTest
class MemberServiceIT {

   @Autowired
   private MemberService memberService;

   @BeforeEach
   private void clearData() {
      memberService.deleteAll();
   }

   @Test
   void save() {
      Member subject = getSubject();
      Member savedSubject = memberService.save(subject);
      List<Member> all = memberService.findAll();
      assertEquals(1, all.size());
      Long id = savedSubject.getId();
      assertNotNull(savedSubject);
      Optional<Member> subjectOptional = memberService.findById(id);
      assertTrue(subjectOptional.isPresent());
      assertEquals(subject, subjectOptional.get());
   }

   @Test
   void createStream() {
      // given
      SubjectStream stream = getStream();

      // when
      SubjectStream created = memberService.createStream(stream);
      Optional<SubjectStream> subjectOptional = memberService.findStreamById(created.getId());

      // then
      assertTrue(subjectOptional.isPresent());
      SubjectStream found = subjectOptional.get();
      assertEquals(stream, found);
      assertTrue(stream.getMandatorySubjects().containsAll(found.getMandatorySubjects()));
   }

   private SubjectStream getStream() {
      return new SubjectStream()
              .setName("Physics")
              .setMandatorySubjects(getMandatorySubjects());
   }

   private List<Member> getMandatorySubjects() {
      return Arrays.asList(
              getSubject()
      );
   }

   private List<ExclusiveSubject> getExclusiveSubjects() {
      return Arrays.asList(getExclusiveSubject());
   }

   private ExclusiveSubject getExclusiveSubject() {
      return new ExclusiveSubject()
              .setAvailableSubjects(Arrays.asList(getSubject()));
   }

   private Member getSubject() {
      return Member.builder()
              .name("Mthokozisi")
              .surname("Mhlanga")
              .phone("Subject description")
              .build();
   }
}